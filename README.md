# GTFS Parser

A tiny GTFS parser/writer

Does not many checks but small and fast. Should work with lots of GTFS feeds.

## Usage

```python
data = gtfsparser.parse("some_gtfs.zip")
data["routes"]["route_a"]["route_color"] = "FF0000"
gtfsparser.dump(data, "some_modified_gtfs.zip")
```

Compound items are grouped together (shapes, stop_times...)

```python
data["shapes"]["1"]
# yields (notice the enclosing list)
# [
#     {
#         'shape_id': '1'
#         'shape_pt_sequence': '1',
#         'shape_pt_lon': '1.2',
#         'shape_pt_lat': '2.3',
#     }, {
#         'shape_id': '1'
#         'shape_pt_sequence': '2',
#         'shape_pt_lon': '1.3',
#         'shape_pt_lat': '2.4',
#     }
# ]
```

## GTFS Filter

The included `gtfsfilter.py` tool allows filtering a GTFS feed, keeping only some trips or routes:

```
./gtfsfilter.py input_gtfs.zip output_gtfs.zip --keep-trips L32* L34*
./gtfsfilter.py input_gtfs.zip output_gtfs.zip --keep-routes 32 34
```

All objects unrelated to the selected ones are removed. More options should be available in the future.
