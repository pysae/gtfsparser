#!/usr/bin/env python3
import io
import zipfile
import csv


# id defaults to table_name[:-1] + '_id'
# repeated defaults to False
_GTFS_REF = {
    'agency': {'id': 'agency_id'},
    'stop_times': {'id': 'trip_id', 'repeated': True, 'seq_field': 'stop_sequence'},
    'calendar': {'id': 'service_id'},
    'calendar_dates': {'id': 'service_id', 'repeated': True},
    'fare_attributes': {'id': 'fare_id'},
    'fare_rules': {'id': 'fare_id', 'repeated': True},
    'shapes': {'repeated': True, 'seq_field': 'shape_pt_sequence'},
    'frequencies': {'id': 'trip_id', 'repeated': True},
    'transfers': {'id': 'from_stop_id', 'repeated': True},
    'feed_info': {'id': 'feed_publisher_name'}
}


def _bom_stripper(text_file):
    yield next(text_file).lstrip("\ufeff")
    for line in text_file:
        yield line


def _get_table(archive, name):
    csvfile = archive.open(name, 'r')
    csvfile = io.TextIOWrapper(csvfile, encoding='utf-8')
    csvfile = _bom_stripper(csvfile)
    return csv.reader(csvfile)


def _table_to_bytes(table):
    string_io = io.StringIO()
    csv_writer = csv.writer(string_io, lineterminator='\n')
    csv_writer.writerows(table)
    return string_io.getvalue().encode('utf-8')


def parse(path):
    archive = zipfile.ZipFile(path)
    result = {}
    for arcname in archive.namelist():
        if not arcname.endswith(".txt"):
            continue
        if "/" in arcname:
            continue
        table_name = arcname[:-4]
        id_field = _GTFS_REF.get(table_name, {}).get('id', table_name[:-1] + '_id')
        repeated = _GTFS_REF.get(table_name, {}).get('repeated', False)
        result[table_name] = {}
        table = _get_table(archive, arcname)
        header = next(table)
        for line in table:
            line = dict(zip(header, line))
            if repeated:
                if line[id_field] not in result[table_name]:
                    result[table_name][line[id_field]] = []
                result[table_name][line[id_field]].append(line)
            else:
                result[table_name][line[id_field]] = line
        seq_field = _GTFS_REF.get(table_name, {}).get('seq_field', None)
        if seq_field is not None:
            for seq in result[table_name].values():
                seq.sort(key=lambda x: int(x[seq_field]))
    return result


def _flatten(data):
    for line in data:
        if isinstance(line, list):
            for subline in line:
                yield subline
        else:
            yield line


def dump(data, path):
    archive = zipfile.ZipFile(path, mode='w', compression=zipfile.ZIP_DEFLATED)
    for table_name in data:
        table = []
        header = []
        for line in _flatten(data[table_name].values()):
            header += list(line.keys() - set(header))
        if header == []:
            continue
        table.append(header)
        for line in _flatten(data[table_name].values()):
            table.append([line.get(k, '') for k in header])
        archive.writestr(table_name + ".txt", _table_to_bytes(table))


if __name__ == '__main__':
    import sys
    import test
    test.feed_info(sys.argv[1])
