#!/usr/bin/env python3
import sys
import time


import gtfsparser


def feed_info(path):
    import time
    start = time.time()
    data = gtfsparser.parse(path)
    print("# {} parsed in {:.3f}s".format(path, time.time() - start))
    print("# Memory size: {:.1f} MiB".format(_recursize(data) / 1048576))
    for table in data:
        print("{}: {}".format(table, len(data[table])))
    return data


def _recursize(obj):
    total = 0
    if isinstance(obj, list):
        for child in obj:
            total += _recursize(child)
    elif isinstance(obj, dict):
        for k, v in obj.items():
            total += sys.getsizeof(k)
            total += _recursize(v)
    total += sys.getsizeof(obj)
    return total


if __name__ == '__main__':
    import glob
    import os

    for gtfs_path in glob.glob("samples/*.zip"):
        data = feed_info(gtfs_path)
        dump_path = gtfs_path + ".dump"
        start = time.time()
        gtfsparser.dump(data, dump_path)
        print("# Dumped in {:.3f}s".format(time.time() - start))
        o_size = os.stat(gtfs_path).st_size / 1024
        d_size = os.stat(dump_path).st_size / 1024
        print("# Dumped size / original size: {:.1f}KiB / {:.1f}KiB ({:.0%})".format(d_size, o_size, d_size / o_size))
        os.remove(dump_path)
        print()
