#!/usr/bin/env python3
import fnmatch

import gtfsparser


def _has(line, field):
    return field in line and line[field]


def _keep(table, ids):
    for eid in table.keys() - ids:
        del table[eid]


def keep_trips(sched, trips):
    trips = set(trips)

    routes = set()
    services = set()
    shapes = set()
    for trip in sched["trips"].values():
        if trip["trip_id"] in trips:
            routes.add(trip["route_id"])
            services.add(trip["service_id"])
            if _has(trip, "shape_id"):
                shapes.add(trip["shape_id"])
    _keep(sched["trips"], trips)
    _keep(sched["stop_times"], trips)
    if "frequencies" in sched:
        _keep(sched["frequencies"], trips)
    _keep(sched["routes"], routes)
    if "calendar" in sched:
        _keep(sched["calendar"], services)
    if "calendar_dates" in sched:
        _keep(sched["calendar_dates"], services)
    if "shapes" in sched:
        _keep(sched["shapes"], shapes)

    agencies = set()
    agencies_keep_all = False
    for route in sched["routes"].values():
        if _has(route, "agency_id"):
            agencies.add(route["agency_id"])
        else:
            agencies_keep_all = True
            break
    if not agencies_keep_all:
        _keep(sched["agency"], agencies)

    stops = set()
    for trip_st in sched["stop_times"].values():
        for st in trip_st:
            stops.add(st["stop_id"])
    stations = set()
    for stop_id in stops:
        if _has(sched["stops"][stop_id], "parent_station"):
            stations.add(sched["stops"][stop_id]["parent_station"])
    stops = stops | stations
    _keep(sched["stops"], stops)

    if "transfers" in sched:
        _keep(sched["transfers"], stops)
        filtered = {}
        for from_stop_id, transfers in sched["transfers"].items():
            ft = []
            for transfer in transfers:
                if transfer["to_stop_id"] in stops:
                    ft.append(transfer)
            filtered[from_stop_id] = ft
        sched["transfers"] = filtered


def keep_trips_pattern(sched, trip_id_pattern):

    trips = set()
    for trip in sched["trips"].values():
        if any(fnmatch.fnmatch(trip["trip_id"], pattern) for pattern in trip_id_pattern):
            trips.add(trip["trip_id"])
    return keep_trips(sched, trips)


def keep_routes(sched, routes):
    routes = set(routes)
    trips = set()
    for trip in sched["trips"].values():
        if trip["route_id"] in routes:
            trips.add(trip["trip_id"])
    return keep_trips(sched, trips)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Filter GTFS files.')
    parser.add_argument('input_file', help='input GTFS file')
    parser.add_argument('output_file', help='output GTFS file')
    filters = parser.add_mutually_exclusive_group()
    filters.add_argument('--keep-trips', nargs='+', metavar='TRIP_ID',
                         help='keep only these trips (accept wildcards)')
    filters.add_argument('--keep-routes', nargs='+', metavar='ROUTE_ID',
                         help='keep only these routes')
    args = parser.parse_args()
    sched = gtfsparser.parse(args.input_file)

    if args.keep_trips:
        keep_trips_pattern(sched, args.keep_trips)
        gtfsparser.dump(sched, args.output_file)

    if args.keep_routes:
        keep_routes(sched, args.keep_routes)
        gtfsparser.dump(sched, args.output_file)
