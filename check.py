#!/usr/bin/env python3
import gtfsparser


def check_stop_times(sched):
    for trip_id, sts in sched["stop_times"].items():

        if len(sts) < 2:
            print("stop_times error in trip {}, there must be at least 2 "
                  "stop_times".format(trip_id))

        sts.sort(key=lambda x: int(x["stop_sequence"]))

        if sts[-1]["departure_time"] <= sts[0]["arrival_time"]:
            print("stop_times error in trip {}, first arrival_time == last "
                  "departure_time".format(trip_id))

        t = None
        d = None
        for st in sts:
            # ascending *_time
            if t is not None and st["arrival_time"] < t:
                print("stop_times error in trip {}, arrival_time < previous "
                      "departure_time on stop_sequence {}".format(trip_id, st["stop_sequence"]))
            t = st["arrival_time"]
            if t is not None and st["departure_time"] < t:
                print("stop_times error in trip {}, departure_time < "
                      "arrival_time on stop_sequence {}".format(trip_id, st["stop_sequence"]))
            t = st["departure_time"]
            # ascending shape_distance_traveled
            if "shape_distance_traveled" in st:
                if d is not None and float(st["shape_distance_traveled"]) < d:
                    print("stop_times error in trip {}".format(trip_id))
                d = float(st["shape_distance_traveled"])
            # existing stop
            if st["stop_id"] not in sched["stops"]:
                print("stop_times error in trip {}, unknown stop_id on "
                      "stop_sequence {}".format(trip_id, st["stop_sequence"]))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Filter GTFS files.')
    parser.add_argument('input_file', help='input GTFS file')

    args = parser.parse_args()
    sched = gtfsparser.parse(args.input_file)

    check_stop_times(sched)
